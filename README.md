# loop

Runs a given process repeatedly.

## Installation

```
cargo install loop --force --git=https://gitlab.com/Fraser999/loop
```

## Usage

From `loop -h`:

```
USAGE:
    loop [FLAGS] [OPTIONS] <PROCESS>...

FLAGS:
    -a, --allow-failures    The process will be repeated even if it fails.
    -h, --help              Prints help information.
    -q, --quiet             By default, the process's output is displayed on each iteration.  Use
                            this flag to suppress the output.  Use this flag twice (pass '-qq') to
                            produce minimal output from 'loop', or thrice (pass '-qqq') for no
                            output.  If the process fails, the output will still be shown for that
                            execution only.
    -V, --version           Prints version information.

OPTIONS:
    -c, --count <ITERATIONS>    Repeats the given process <ITERATIONS> times or until the process
                                returns a non-zero value and '--allow-failures' is not used.
                                <ITERATIONS> must be a positive integer.  If this option is not
                                provided, the process will be repeated indefinitely.

ARGS:
    <PROCESS>...    Process to run, along with any arguments it takes.
```

For example, to run tests 1000 times in a separate target directory, stopping if any fails:

```
loop -qc1000 cargo test --target-dir=target/soak-test
```

## License

`loop` is distributed under the terms of the General Public License (GPL), version 3
([COPYING](COPYING) or http://www.gnu.org/licenses/gpl-3.0.en.html).
